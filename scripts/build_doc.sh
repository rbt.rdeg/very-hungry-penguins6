#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")/../"

  # shellcheck disable=SC2046
  eval $(opam config env)

  oasis setup > /tmp/null

  make configure > /tmp/null
  make doc
)
