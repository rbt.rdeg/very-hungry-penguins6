(** Game module *)

(** game type: game_name, map_fileName (.json or .txt file) * (playerType list) * client list *)
type t = string * string * (string list) * (int list) * (Network.client list) (* TODO: replace (int list) by (playerType list) ; add a game state ? *)
